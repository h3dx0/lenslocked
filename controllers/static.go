package controllers

import (
	"html/template"
	"net/http"

	"gitlab.com/h3dx0/lenslocked/views"
)

type Static struct {
	Template views.Template
}

func StaticHandler(tpl views.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tpl.Execute(w, nil)
	}
}

func FAQ(tpl views.Template) http.HandlerFunc {
	questions := []struct {
		Question string
		Answer   template.HTML
	}{
		{
			Question: "What is this thing?",
			Answer:   "This is a simple image gallery web application written in Go.",
		},
		{
			Question: "Who made this?",
			Answer:   "This was built by Jon Calhoun for his upcoming \"Go Web Development\" course.",
		},
		{
			Question: "Why did you make this?",
			Answer:   "To teach people how to make web applications in Go. <a href=\"/contact\">Contact me</a> to learn more!",
		},
	}

	return func(w http.ResponseWriter, r *http.Request) {
		tpl.Execute(w, questions)
	}
}
